﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InnovationScaffolding.Startup))]
namespace InnovationScaffolding
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
